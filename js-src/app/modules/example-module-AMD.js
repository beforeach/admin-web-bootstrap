define(['jquery'], function($) {

  'use strict';

  var exampleModule = {

    'init': function() {
        console.log('AMD Module loaded, $ is ' + $);
    }

  };

  return exampleModule; //public

});
