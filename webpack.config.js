var path = require('path');
var webpack = require('webpack');
var config = require('./gulp/config');

module.exports = {
  'cache': true,
  'context': path.join(__dirname, config.js.src),
  'entry': {
    'main': './projectname',
    'common': ['jquery']
  },
  'output': {
    'path': path.join(__dirname, config.js.dest),
    'publicPath': config.js.dest,
    'filename': '[name].js',
    'chunkFilename': '[chunkhash].js'
  },
  'resolve': {
    'root': path.join(__dirname, config.js.src),
    'modulesDirectories':
      [
        'node_modules',
        'lib/',
        'polyfills/'
      ],
    'alias': {
      // Bind version of jquery
      'jquery': 'jquery-1.12.0'
    }
  },
  /*
  // IF YOU USE a CDN (attach script tag with src="//'//cdn.jsdelivr.net/jquery/1.12.0/jquery.min'"):
  'externals': {
    // require("jquery") is external and available
    //  on the global var jQuery
    "jquery": "jQuery"
  },
  */
  'plugins': [
    new webpack.optimize.CommonsChunkPlugin(/* chunkName= */"common", /* filename= */"common.bundle.js"),
    new webpack.IgnorePlugin(/^\.\/locale$/, [/moment$/]), // saves ~100k
    new webpack.ProvidePlugin({
      // Automatically detect jQuery and $ as free var in modules
      // and inject the jquery library
      // This is required by many jquery plugins
      'jQuery': 'jquery',
      '$': 'jquery',
      'window.jQuery': 'jquery'
    })
  ]
};
