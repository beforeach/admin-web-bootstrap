var root = '';
var dest = root + '';
var src = root + '';
var gutil = require('gulp-util');

module.exports = {
    'publicURL': 'foreach.be',
    'devURL': 'foreach.local',
    'templates': ['**/*.html', '**/*.tpl.php'],
    'scss': {
        'src': [src + (gutil.env.path?gutil.env.path:'') + '**/*.scss', src + '!node_modules/**/*.scss'],
        'dest': dest + (gutil.env.path?gutil.env.path:'')
    },
    'js': {
        'src': src + 'js-src/',
        'dest': src + 'js/'
    },
    'svg': {
        'src': src + 'svg-src/',
        'dest': src + 'svg/'
    },
    'images_src': 'static/**/*',
    'images_dest': 'static/',
    'lint': {
        'js': {
            'src': [
                src + '/**/*.js',
                '!' + src + '/**/lib/**/*.js'
            ],
            'config': root + '/.jshintrc'
        },
        'scss': {
            'src': [
                src + (gutil.env.path?gutil.env.path:'') + '**/*.scss'
            ]
        }
    }
};
