gulp watch --path="folder/subfolder"
(or gulp scss --path="folder/subfolder")

-> will scope our watch to a sub path
-> can be used to compile or watch only a specific path
-> the path argument is relative from sites/all/themes/
-> no trailing slash!
