var gulp = require('gulp');
var size = require('gulp-size');
var imagemin = require('gulp-imagemin');
var config = require('../config');
var pngquant = require('imagemin-pngquant');

// Optimize images
gulp.task('images', function () {
	console.log('processing folder ' + config.images_src);

  return gulp.src(config.images_src)
    .pipe(imagemin({
      progressive: true,
      interlaced: true,
      use: [pngquant({quality: '65-80', speed: 4})]
    }))
    .pipe(gulp.dest(config.images_dest))
    .pipe(size({title: 'images'}));
  }
);
