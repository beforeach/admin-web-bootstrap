var gulp = require('gulp');
var config = require('../config');
var path = require('path');
var gutil = require('gulp-util');

gulp.task('watch', ['scss'], function () {
    // scss
    var scssWatcher = gulp.watch(config.scss.src, ['scss']);
    scssWatcher.on('change', function (event) {
        gutil.log( gutil.colors.yellow( 'Scss watcher says ' + path.basename(event.path) ) + ' is changed.' );
    });

    scssWatcher.on('error', function (event) {
        gutil.log( gutil.colors.yellow( 'Error in ' + path.basename(event.path) ) );
        this.emit('end');
    });

});
