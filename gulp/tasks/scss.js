var gulp = require('gulp');
var gutil = require('gulp-util');
var handleErrors = require('../util/handleErrors');
var config = require('../config').scss;
var sass = require( 'gulp-sass' );
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var gulpif = require('gulp-if');

gulp.task('scss', function () {
    console.log( "starting to compile " + config.src );
    return gulp.src(config.src)
    .pipe( gulpif( !gutil.env.production, sourcemaps.init() ) )
    .on('error', handleErrors)
    .pipe(sass({
        outputStyle: gutil.env.production ? 'compressed' : 'expanded',
        errLogToConsole: true,
        sourceComments : gutil.env.production ? false : true
    }))
    .on('error', handleErrors)
    .pipe(autoprefixer())
    .on('error', handleErrors)
    .pipe( gulpif( !gutil.env.production, sourcemaps.write() ) )
    .on('error', handleErrors)
    .pipe(gulp.dest(config.dest))
});
