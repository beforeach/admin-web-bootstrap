var gulp= require('gulp');
var gutil = require('gulp-util');
var path = require('path');
var browserSync = require('browser-sync').create();
var config = require('../config');

const reload = browserSync.reload;

// Static server
gulp.task('serve', ['scss'], function() {

  var browserSyncConfig = {};

  if( gutil.env.proxy ) {
    console.log( 'INFO: proxying ' + config.devURL );
    browserSyncConfig.proxy = config.devURL;
  } else {
    console.log( 'INFO: you can use --proxy to route through your application/site' );
    console.log( 'INFO: you can use --port PORTNUMBER' );
    browserSyncConfig.server = {
      'notify': true,
      // Customize the Browsersync console logging prefix
      'logPrefix': 'FE',
      'baseDir': './',
      'port': gutil.env.port? gutil.env.port: 3000
    };
  }

  browserSync.init( browserSyncConfig );

  gulp.watch( config.templates, reload ); // watch for changes in templates

  // watch for changes in scss
  var scssWatcher = gulp.watch( config.scss.src, ['scss', reload] );

  scssWatcher.on( 'change', function ( event ) {
    gutil.log( gutil.colors.yellow( 'Scss watcher says ' + path.basename( event.path ) ) + ' changed.' );
  });

  scssWatcher.on( 'error', function ( event ) {
    gutil.log( gutil.colors.yellow( 'Error in ' + path.basename( event.path ) ) );
    this.emit( 'end' );
  });

});
