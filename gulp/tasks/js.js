var gulp = require("gulp");
var gutil = require("gulp-util");
var config = require('../config');
var webpack = require("webpack");
var webpackConfig = require("../../webpack.config.js");
var gulpif = require('gulp-if');

// base config, to be extended
var myConfig = Object.create(webpackConfig);

gulp.task("js", function(callback) {
  // if we are production, single compile
  // if we are dev, watch with sourcemaps

  if (gutil.env.watch) {
    myConfig.watch = true;
    console.log('WATCHING');
  } else {
    console.log('NOT WATCHING (use --watch)');
  }

  if (gutil.env.production) {

    myConfig.plugins = myConfig.plugins.concat(
      new webpack.DefinePlugin({
        "process.env": {
          // This has effect on the react lib size
          "NODE_ENV": JSON.stringify("production")
        }
      }),
      new webpack.optimize.DedupePlugin(),
      new webpack.optimize.UglifyJsPlugin()
    );

  } else {
    myConfig.devtool = "inline-source-map";
    myConfig.debug = true;
    myConfig.cache = false;
  }

  // run webpack
  return webpack(myConfig, function(err, stats) {
    if (err) throw new gutil.PluginError("webpack:build", err);
    gutil.log("[webpack:build]", stats.toString({
      colors: true
    }));
  });

});
