
var gulp = require('gulp');
var svgSprite = require('gulp-svg-sprite');
var config = require('../config').svg;
var path = require('path');
var glob = require('glob');
var size = require('gulp-size');

gulp.task('svg', function() {
  var svgDest = config.dest;

  return glob(config.src, function(err, dirs) {
    dirs.forEach(function(dir) {
      gulp.src( path.join(dir, '*.svg') )
      .pipe( svgSprite(makeSvgSpriteOptions(dir)) )
      .pipe( size({ showFiles: true, title: svgDest }) )
      .pipe( gulp.dest(svgDest) )
    })
  });

  function makeSvgSpriteOptions(dirPath) {
    return {
      mode: {
        symbol: {
          dest: '.',
          example: true,
          sprite: 'main.svg'
        },
        // css : {
        //   dest:'.',
        //   render : {
        //     css : {dest:'./css/sprite.css'},
        //     scss : {
        //       dest : './scss/modules/_m-svg-icons.scss'
        //     }
        //   }
        // }
      }
    };
  }
});
